import Label from "./label";
import Input from "./input";

const inputForm = (props) => {
  const { label, name, type, placeholder, variant, className } = props;
  return (
    <div className="mb-5">
      <Label htmlFor={name} variant={variant}>
        {label}
      </Label>
      <Input
        type={type}
        placeholder={placeholder}
        name={name}
        className={className}
      />
    </div>
  );
};

export default inputForm;
