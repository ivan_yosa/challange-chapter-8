const Input = (props) => {
    const { type, placeholder, name, className } = props;
    return (
      <input
        type={type}
        // className="text-sm w-full border rounded placeholder-black opacity-50 p-2"
        className={`text-sm w-full border rounded placeholder-black opacity-50 p-2 ${className}`}
        placeholder={placeholder}
        name={name}
        id={name}
      />
    );
  };
  
  export default Input;
  