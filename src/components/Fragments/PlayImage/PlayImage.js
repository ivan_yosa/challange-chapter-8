import "./PlayImage.css";

const PlayImage = (props) => {
  const { src, id, classname, onClick } = props;
  return (
    <img
      src={src}
      alt={id}
      id={id}
      className={`rpsChoice my-10 ml-5 hover:scale-110 hover:cursor-pointer transition-all p-2 rounded-xl ${classname}`}
      onClick={onClick}
    />
  );
};
export default PlayImage;
