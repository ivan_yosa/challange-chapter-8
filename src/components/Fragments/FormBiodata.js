import InputForm from "../input/index";

const FormBiodata = () => {
    return (
      <form action="">
        <InputForm
          label="Fullname"
          name="fullname"
          type="text"
          placeholder="insert your name here"
        />
        <InputForm
          label="Address"
          name="address"
          type="text"
          placeholder="insert address here"
        />
        <InputForm
          label="PhoneNumber"
          name="PhoneNumber"
          type="number"
          placeholder="insert your phone number here"
        />
      </form>
    );
  };
  export default FormBiodata;
  