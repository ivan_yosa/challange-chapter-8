import Button from "./button";

const Kanan = () => {
  return (
    <div
      style={{
        backgroundColor: "lightblue",
        width: "50vw",
        padding: "20px",
      }}
    >
      Kanan
      <div>
        <Button title="Klik ME!" />
      </div>
    </div>
  );
};

export default Kanan;
