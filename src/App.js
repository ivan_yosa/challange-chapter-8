import "./App.css";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import AllRooms from "./pages/AllRooms";
import GameHistory from "./pages/GameHistory";
import CreateRooms from "./pages/createRooms/createRooms";
import VsCom from "./pages/vsCom/vsCom";
import VsPlayer from "./pages/playerVsplayer/pvp";
import Navbar from "./pages/Navbar";
import { Route, Routes } from "react-router-dom";

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Navbar />}>
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="/allRooms" element={<AllRooms />} />
        <Route path="/createRooms" element={<CreateRooms />} />
        <Route path="/gameHistory" element={<GameHistory />} />
        <Route path="/vsCom" element={<VsCom />} />
        <Route path="/vsPlayer" element={<VsPlayer />} />
      </Route>
    </Routes>
  );
};

export default App;
