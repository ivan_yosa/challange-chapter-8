import { Link, Outlet } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="bg-slate-700 flex flex-col w-full h-screen">
      <nav className="flex p-5 text-white bg-orange-600 justify-between items-center cursor-pointer">
        <div>
          <span className="text-2xl font-bold">Rock Paper Scissors</span>
        </div>
        <ul className="flex items-center justify-between gap-10">
          <li className="hover:scale-110 hover:cursor-pointer transition-all">
            <Link
              to={"/Login"}
              className="text-xl text-white hover:text-black duratin-500"
            >
              Sign In
            </Link>
          </li>
          <li className="hover:scale-110 hover:cursor-pointer transition-all">
            <Link
              to={"/Registration"}
              className="text-xl text-white hover:text-black duratin-500"
            >
              Sign Up
            </Link>
          </li>
          <li className="hover:scale-110 hover:cursor-pointer transition-all">
            <Link
              to={"/AllRooms"}
              className="text-xl text-white hover:text-black duratin-500"
            >
              MAIN MENU
            </Link>
          </li>
        </ul>
      </nav>
      <Outlet />
    </div>
  );
};

export default Navbar;
